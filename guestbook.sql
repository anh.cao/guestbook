/*
Navicat MySQL Data Transfer

Source Server         : DB-LOCAL2
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : guestbook

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-03-28 11:18:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(255) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 08:29:05', '2019-03-27 02:29:05');
INSERT INTO `comment` VALUES ('4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 08:41:16', '2019-03-27 02:41:16');
INSERT INTO `comment` VALUES ('5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 08:43:40', '2019-03-27 02:43:40');
INSERT INTO `comment` VALUES ('6', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 08:50:12', '2019-03-27 02:50:12');
INSERT INTO `comment` VALUES ('9', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 10:45:54', '2019-03-27 04:45:54');
INSERT INTO `comment` VALUES ('15', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 17:20:35', '2019-03-27 11:20:35');
INSERT INTO `comment` VALUES ('20', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 23:11:34', '2019-03-27 17:11:34');
INSERT INTO `comment` VALUES ('21', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 23:33:45', '2019-03-27 17:33:45');
INSERT INTO `comment` VALUES ('24', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 23:41:38', '2019-03-27 17:41:38');
INSERT INTO `comment` VALUES ('25', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 23:41:42', '2019-03-27 17:41:42');
INSERT INTO `comment` VALUES ('26', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 23:41:47', '2019-03-27 17:41:47');
INSERT INTO `comment` VALUES ('27', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 23:41:52', '2019-03-27 17:41:52');
INSERT INTO `comment` VALUES ('28', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-27 23:42:00', '2019-03-27 17:42:00');
INSERT INTO `comment` VALUES ('29', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-28 00:02:44', '2019-03-27 18:02:44');
INSERT INTO `comment` VALUES ('30', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-28 09:38:37', '2019-03-28 03:38:37');
INSERT INTO `comment` VALUES ('31', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-28 10:30:05', '2019-03-28 04:30:05');
INSERT INTO `comment` VALUES ('32', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-28 11:03:11', '2019-03-28 05:03:11');
INSERT INTO `comment` VALUES ('34', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 'Hoang Anh Cao', 'caohoanganhuit@gmail.com', '2019-03-28 11:13:21', '2019-03-28 05:13:21');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pasword` varchar(255) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `ac` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '$2b$10$1er4wPbth3qHV66.czWOuO3FgSNWLts9vh0BfI.stxeAH2l8y4ptm', 'admin@yopmail.com', 'admin', '1');
