<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/27/2019
 * Time: 2:45 PM
 */

class User extends BaseModel
{

    /**
     * @param $data
     * @return array
     */
    public function checkUser($data)
    {
        $username = $data['username'];
        $pwd = $data['password'];
        $sql = "SELECT * FROM user where `username`= '".$username."' LIMIT 1";
        $req = $this->db->query($sql);
        $data = $req->fetch(PDO::FETCH_ASSOC);
        if(!password_verify($pwd, $data['pasword'])){
            return ['sts' => false, 'message' => 'Login Failed!'];
        }
        return $data;
    }
}