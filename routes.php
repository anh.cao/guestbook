<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/25/2019
 * Time: 10:36 PM
 */

/**
 * Route here
 */
$controllers = [
    //controller  => 'pages belong to that controller'
    'pages' => ['home', 'error', 'postComment','getDetail','deleteComment','postLogin'],

    // add more controller here
    //
];

if (!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])) {
    $controller = 'pages';
    $action = 'error';
}

$file = 'controllers/' . str_replace('_', '', ucwords($controller, '_')) . 'Controller.php';
if(file_exists($file)){
    $klass = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
    $controller = new $klass();
    $controller->$action();
}else {
    echo "Route not found";
}

