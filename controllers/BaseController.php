<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/25/2019
 * Time: 11:39 PM
 */
class BaseController
{
    protected $folder;

    protected function view($file, $data = [])
    {
        $view_file = 'views/' . $this->folder . '/' . $file . '.php';
        if (is_file($view_file)) {

            extract($data);
            ob_start();
            require_once($view_file);
            $content = ob_get_clean();

            require_once('views/layouts/application.php');
        } else {
            header('Location: index.php?controller=pages&action=error');
        }
    }

    public function error(){
        $this->view('error');
    }

    /**
     * @param $status_code
     * @param null $data
     */
    protected function response($status_code, $data = NULL){
        header($this->_build_http_header_string($status_code));
        header("Content-Type: application/json");
        echo json_encode($data);
        die();
    }

    /**
     * Create http header
     * @param: $status_code: code http
     * @return:
     */
    private function _build_http_header_string($status_code){
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error'
        );
        return "HTTP/1.1 " . $status_code . " " . $status[$status_code];
    }
}