//admin variable global
var admin = null;

$(document).ready(function() {
    $('#sendCommentModalForm').click(function () {

        //validate form
        let full_name = $('#full-name').val();
        if(!full_name){
            $('#full-name-err').text('Full name is required.')
            return false;
        }
        let message_text = $('#message-text').val();

        if(!message_text){
            $('#message-text-err').text('Message is required.');
            return false;
        }
        if(message_text.length > 255){
            $('#message-text-err').text('The length of Message is 255 character max.');
            return false;
        }
        let url = "?controller=pages&action=postComment";
        let data = {
            params : {
                id : $('#comment-id').val(),
                full_name : full_name,
                email :  $('#email').val(),
                message_text :  message_text
            },
            dataType: 'json',
            method : 'POST'
        };
        callAjax(url,data,function (res) {
            //close the modal
            $('#closeCommentModalForm').click();
            //refresh the message area
            refreshContentMessageArea(res);
        })
    });

    /**
     * Login action
     */
    $('#sendLoginModalForm').click(function () {
        let url = "?controller=pages&action=postLogin";
        let data = {
            params : {
                username : $('#username').val(),
                password : $('#password').val(),
            },
            dataType: 'json',
            method : 'POST'
        };
        callAjax(url,data,function (res) {
            //close the modal
            $('#closeLoginModalForm').click();
            if(res['role'] == 'admin'){
                //set admin after login
                admin = res;
                $('.admin-visible').css({'display': 'block'});
                let html = '<a class="btn btn-block">Hello, ' + res['username']+ '</a>';
                $('#adminLoginBtn').replaceWith(html);
            }

        })
    });
});

/**
 * Call Ajax
 * @param url
 * @param data
 * @param callback
 */
function callAjax(url, data, callback) {
    $.ajax({
        url: url,
        data : data['params'],
        dataType : data['dataType'],
        method: data['method'],
        context: document.body
    }).done(function(res) {
        callback(res);
    }).fail(function(err) {
        if(err.responseJSON){
            alert(err.responseJSON['message']);
        }
    });
}

/**
 * Set default value
 * @param data
 */
function setValueCommentModalForm(data = {}) {
    $('#comment-id').val(data['id']? data['id'] :'');
    $('#full-name').val(data['full_name']? data['full_name'] :'');
    $('#email').val(data['email']? data['email'] :'');
    $('#message-text').val(data['desc']? data['desc'] :'')
    if(data['id']){
        $('#full-name').prop('readonly', true);
        $('#email').prop('readonly', true);
    }else {
        $('#full-name').prop('readonly', false);
        $('#email').prop('readonly', false);
    }
}

/**
 * Edit Guest comment
 * @param id
 */
function editItem(id) {
    let url = "?controller=pages&action=getDetail";
    let data = {
        params : {
            id : id
        },
        dataType: 'json',
        method : 'GET'
    };
    callAjax(url, data, function (res) {
        $('#commentModalLabel').text('Edit Comment');
        $('#sendCommentModalForm').text('Save');
        setValueCommentModalForm(res);
    });
    $('#commentModal').modal();
}

/**
 * Delete comment
 * @param id
 */
function deleteItem(id){
    let url = "?controller=pages&action=deleteComment";
    let data = {
        params : {
            id : id
        },
        dataType: 'json',
        method : 'POST'
    };
    callAjax(url, data, function (res) {
        refreshContentMessageArea(res);
    });
}

function setDefaultModal(){
    //set form to default value
    setValueCommentModalForm()
    $('#commentModalLabel').text('New Comment');
    $('#sendCommentModalForm').text('Send Comment');
    $('#full-name-err').text('');
    $('#message-text-err').text('');
}

function refreshContentMessageArea(res) {
    let html = '<div class="row list-message" id="content-message-area">';
    for (let item of res['list']){
        html+= '<div class="col-xs-12 col-sm-6 col-md-6 comment">';
        html+= '<div class="comment-content">';
        html+= '    <p>'+ item['desc'] +'</p>';
        html+= '</div>';
        html+= '<div class="comment-footer">';
        html+= '    <div>';
        html+= '        <h6 class="comment-sign">'+ item['full_name'] +'</h6>';
        html+= '        <p>' + item['created_at'] + '</p>';
        html+= '    </div>';
        html+= '    <div class="pull-right admin-visible">';
        html+= '        <a class="btn btn-danger btn-default" onclick="editItem('+ item['id'] +')"><i class="fa fa-pen"></i></i></a>';
        html+= '        <a class="btn btn-danger btn-default" onclick="deleteItem('+ item['id'] +')"><i class="fas fa-trash" ></i></a>';
        html+= '    </div>';
        html+= '</div>';
        html+= '</div>';
    }
    html+='</div>';
    $("#content-message-area").replaceWith(html);

    if(admin != null){
        $('.admin-visible').css({'display': 'block'});
    }
}