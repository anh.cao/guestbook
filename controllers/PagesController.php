<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/25/2019
 * Time: 11:42 PM
 */

class PagesController extends BaseController
{
    protected $guestBookModel;

    protected $userModel;

    public function __construct()
    {
        $this->folder = 'pages';
        $this->guestBookModel = new GuestBook(DB::getInstance());
        $this->userModel = new User(DB::getInstance());
    }

    public function home()
    {
        $data = $this->guestBookModel->getAll($_REQUEST);
        $this->view('home', $data);
    }

    /**
     * Only Post method is allowed
     */
    public function postComment(){
        if(empty($_POST)){
            header('Location: index.php?controller=pages&action=error');
        }
        $data = $_POST;
        try{
            if($data['id']){
                $result = $this->guestBookModel->updateComment($data['id'], $data, $_POST);
            }else{
                $result = $this->guestBookModel->createComment($data, $_POST);
            }
        }catch (PDOException $exception){
            return $this->response(500, $exception->getMessage());
        }
        return $this->response(200, $result);
    }

    /**
     *
     */
    public function getDetail(){
        if(empty($_GET['id'])){
            header('Location: index.php?controller=pages&action=error');
        }
        try{
            $result = $this->guestBookModel->getCommentDetail($_GET['id']);
        }catch (PDOException $exception){
            return $this->response(500, $exception->getMessage());
        }
        return $this->response(200, $result);
    }

    /**
     * Delete Comment
     */
    public function deleteComment(){
        if(empty($_POST['id'])){
            header('Location: index.php?controller=pages&action=error');
        }
        try{
            $result = $this->guestBookModel->deleteComment($_POST['id'], $_POST);
        }catch (PDOException $exception){
            return $this->response(500, $exception->getMessage());
        }
        return $this->response(200, $result);
    }

    /**
     * Login action
     */
    public function postLogin(){
        if(empty($_POST)){
            header('Location: index.php?controller=pages&action=error');
        }
        $data = $_POST;

        try{
            $result = $this->userModel->checkUser($data);
            if(isset($result['sts']) && !$result['sts']){
                return $this->response(500, $result);
            }
        }catch (PDOException $exception){
            return $this->response(500, $exception->getMessage());
        }
        return $this->response(200, $result);
    }

}