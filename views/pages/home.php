<div class="container-inside">
    <div class="row">
        <div class="col-md-2" style="position: relative">
            <img class="logo" src="./assets/images/logo/logo-hae-group-large.png">
            <div class="logo-line"></div>
            <h4 class="logo-text">Guestbook</h4>
            <p class="logo-tex-decoration">Feel free to leave us a short message to tell us what you think to our services</p>
            <div class="row" style="margin: auto;">
                <button class="btn btn-danger" data-toggle="modal" data-target="#commentModal" onclick="setDefaultModal()">Post a Message</button>
            </div>
            <div class="row btn-block-row">
                <a class="btn btn-block" data-toggle="modal" data-target="#loginModal" id="adminLoginBtn">Admin Login</a>
            </div>
        </div>
        <div class="col-md-10 content" >
            <div class="row list-message" id="content-message-area">
                <?php foreach ($data['list'] as $comment) {?>
                    <div class="col-xs-12 col-sm-6 col-md-6 comment">
                        <div class="comment-content">
                            <p><?= $comment['desc'] ?></p>
                        </div>
                        <div class="comment-footer">
                            <div>
                                <h6 class="comment-sign"><?= $comment['full_name'] ?></h6>
                                <p><?= $comment['created_at'] ?></p>
                            </div>
                            <div class="pull-right admin-visible">
                                <a class="btn btn-danger btn-default" onclick="editItem(<?=$comment['id']?>)"><i class="fa fa-pen"></i></i></a>
                                <a class="btn btn-danger btn-default" onclick="deleteItem(<?=$comment['id']?>)"><i class="fas fa-trash" ></i> </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-md-12 pagination-comment">
                    <?php
                    $meta = $data['meta'];
                    //show Prev
                    if ($meta['current_page'] > 1 && $meta['total_page'] > 1){
                        echo '<a href="index.php?page='.($meta['current_page']-1).'"><i class="fa fa-angle-left"></i></a>';
                    }

                    for ($i = 1; $i <= $meta['total_page']; $i++){

                        if ($i == $meta['current_page']){
                            echo '<span>'.$i.'</span>';
                        }
                        else{
                            echo '<a href="index.php?page='.$i.'">'.$i.'</a>';
                        }
                    }
                    //Show next
                    if ($meta['current_page'] < $meta['total_page'] && $meta['total_page'] > 1){
                        echo '<a href="index.php?page='.($meta['current_page']+1).'"><i class="fa fa-angle-right"></i></a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="commentModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="commentModalForm" method="post" action="?controller=pages&action=postComment">
                    <input type="hidden" id="comment-id" value="">
                    <div class="form-group">
                        <label for="full-name" class="col-form-label">Full Name <span class="text-danger">(*)</span>:</label>
                        <p class="text-danger" id="full-name-err"></p>
                        <input type="text" class="form-control" id="full-name" name="full-name" required>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email:</label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Message <span class="text-danger">(*)</span>:</label>
                        <p class="text-danger" id="message-text-err"></p>
                        <textarea class="form-control" id="message-text" name="message-text" maxlength="255"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeCommentModalForm">Close</button>
                <button type="button" class="btn btn-primary" id="sendCommentModalForm">Send message</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginModalLabel">Sign In</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="username" class="col-form-label" id="">User Name:</label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label">Password:</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeLoginModalForm">Close</button>
                <button type="button" class="btn btn-primary" id="sendLoginModalForm">OK</button>
            </div>
        </div>
    </div>
</div>



