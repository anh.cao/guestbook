<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/25/2019
 * Time: 10:34 PM
 */
return [
    'host'          => 'localhost',
    'database_name' => 'guestbook',
    'username'      => 'root',
    'password'      => '',
    'port'          => '3306'
];