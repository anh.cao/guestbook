<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/25/2019
 * Time: 10:32 PM
 */
function __autoload($className)
{
    if(file_exists($className . '.php')){
        include_once $className . '.php';
    }
}
require_once('constants.php');
require_once('connection.php');

$get = $_GET; // set global variable get

if(isset($get['controller']) && $get['controller']){
    $controller = $get['controller'];
    $action ='home'; //set action default

    if(isset($get['action']) && $get['action']){
        $action = $get['action'];
    }
}else{
    $controller = 'pages';
    $action = 'home';
}

/**
 * @param $folder
 * Include all folder class
 */
function include_all_php($folder){
    foreach (glob("{$folder}/*.php") as $filename)
    {
        include_once($filename);
    }
}

$folders = ['controllers', 'models','helpers'];
foreach ($folders as $folder){
    include_all_php($folder);
}
require_once('routes.php');
