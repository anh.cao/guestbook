<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/25/2019
 * Time: 10:36 PM
 */

class DB {
    private static $instance = NULl;
    public static function getInstance() {
        //$config
        $dbConfig = config('databases');
        if (!isset(self::$instance)) {
            try {
                $databaseInfo = 'mysql:host='.$dbConfig['host']
                    .';port='.$dbConfig['port']
                    .';dbname='.$dbConfig['database_name'];
                self::$instance = new PDO($databaseInfo, $dbConfig['username'], $dbConfig['password']);
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instance->exec("SET NAMES 'utf8'");
            } catch (PDOException $ex) {
                die($ex->getMessage());
            }
        }
        return self::$instance;
    }
}