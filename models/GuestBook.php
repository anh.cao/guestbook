<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/26/2019
 * Time: 12:54 AM
 */

class GuestBook extends BaseModel
{
    const PATTERN = "/'/m";

    /**
     * @param $request
     * @return array
     */
    public function getAll($request){
        $params = $request;
        $page = $params['page'] ?? 1;
        $limit = $params['limit'] ?? 6;

        //total record

        $countSql = "SELECT count(id) as total FROM comment";
        $row = $this->db->query($countSql)->fetch(PDO::FETCH_ASSOC);
        $totalRecord = $row['total'];
        $totalPage = ceil($totalRecord / $limit);

        //check page if greater than total page
        if ($page > $totalPage){
            $page = $totalPage;
        }
        else if ($page < 1){
            $page = 1;
        }

        $start = ($page - 1) * $limit;

        $meta = [
            'total_record'  => $totalRecord,
            'total_page'    => $totalPage,
            'current_page'  => $page
        ];

        $list = [];
        $sql = "SELECT * FROM comment ORDER BY id DESC LIMIT $start,$limit";
        $req = $this->db->query($sql);
        foreach ($req->fetchAll() as $item) {
            $list[] = [
                'id'            => $item['id'],
                'desc'          => $item['desc'],
                'full_name'     => $item['full_name'],
                'email'         => $item['email'],
                'created_at'    => date("M j, Y, g:i a", strtotime($item['created_at'])),
                'updated_at'    => date("M j, Y, g:i a", strtotime($item['updated_at']))
            ];
        }
        return [
            'list' => $list,
            'meta' => $meta
        ];
    }

    public function createComment($data, $request)
    {
        $sql = "INSERT INTO comment(`desc`,full_name,email,updated_at) 
                values('".preg_replace(self::PATTERN,"\'", $data['message_text'])."','".$data['full_name']."','".$data['email']."','".date('Y-m-d H:i:s')."')";

        return $this->executeSQL($sql, $request);
    }

    public function updateComment($id, $data, $request)
    {
        $message = preg_replace(self::PATTERN,"\'", $data['message_text']);
        $sql = "UPDATE comment SET `desc` = '".$message."' WHERE id =".$id;
        return $this->executeSQL($sql, $request);
    }

    public function deleteComment($id, $request){
        $sql = "DELETE FROM comment WHERE id =".$id;
        return $this->executeSQL($sql, $request);
    }

    public function executeSQL($sql, $request){
        $this->db->exec($sql);
        $refreshList = $this->getAll($request);
        return $refreshList;
    }

    public function getCommentDetail($id)
    {
        $sql = "SELECT * FROM comment WHERE id =".$id ." LIMIT 1";
        $req = $this->db->query($sql);
       return $req->fetch(PDO::FETCH_ASSOC);
    }
}