<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/27/2019
 * Time: 2:46 PM
 */

class BaseModel
{
    /**
     * @Set DB
     */
    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }
}