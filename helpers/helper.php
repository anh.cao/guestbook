<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 3/26/2019
 * Time: 8:09 AM
 */
if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        return $value;
    }
}

if (! function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (! function_exists('dd')){
    /**
     * Die Dump
     * @param $value
     */
    function dd(...$value){
        print_r('<pre>');
        array_map(function ($value) {
           var_dump($value);
        }, func_get_args());
        print_r('<pre/>');
        die(1);
    }
}

if(! function_exists('bcrypt')){
    /**
     * @param $password
     * @return bool|string
     */
    function bcrypt($password){
        return password_hash($password, PASSWORD_BCRYPT );
    }
}

if(! function_exists('config')){
    /**
     * @param $file
     * @return mixed
     */
    function config($file){
        return require_once ROOT_PATH.'/config/'.$file.'.php';
    }
}
