<DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>GuestBook</title>
        <link rel="stylesheet" type="text/css" href="./assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="./assets/fonts/awesome/css/all.css">

    </head>
    <body >
    <?= @$content ?>

    <script src="./assets/js/jquery.js"></script>
    <script src="./assets/js/script.js"></script>
    <script src="./assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>